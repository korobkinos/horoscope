<?php

/**
 * This is the model class for table "sign_zodiac".
 *
 * The followings are the available columns in table 'sign_zodiac':
 * @property integer $id
 * @property string $timestamp
 * @property integer $input_date
 * @property string $sign
 * @property string $salt
 * @property integer $ip
 * @property string $user_name
 */
class SignZodiac extends CActiveRecord {

    public $signArray = array();
    public $message = array();
    public $ip;
    public $correctdate;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SignZodiac the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sign_zodiac';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_name, message', 'required', 'message' => 'Пустое поле: {attribute}.'),
            array('input_date', 'type', 'type' => 'date', 'message' => '{attribute}: Неправильная дата!', 'dateFormat' => 'd.M.yyyy'),
            array('sign, user_name', 'length', 'max' => 150, 'message' => 'Привышен допустимый лимит {attribute}.'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, timestamp, input_date, sign, salt, ip, user_name, message', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'timestamp' => 'Время создания',
            'input_date' => 'Дата рождения',
            'sign' => 'Знак зодиака',
            'salt' => 'Salt',
            'ip' => 'Ip',
            'user_name' => 'Ваше имя',
            'message' => 'Сообщение',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('timestamp', $this->timestamp, true);
        $criteria->compare('input_date', $this->input_date);
        $criteria->compare('sign', $this->sign, true);
        $criteria->compare('salt', $this->salt, true);
        $criteria->compare('ip', $this->ip);
        $criteria->compare('message', $this->message);
        $criteria->compare('user_name', $this->user_name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getSignArray() {
        $this->signArray = array('овен', 'телец', 'близнецы', 'рак', 'лев', 'дева', 'весы', 'скорпион', 'стрелец', 'козерог', 'водолей', 'рыбы');
        foreach ($this->signArray as $key => $value) {
            $this->signArray[$key + 1] = $value;
            unset($this->signArray[0]);
        }
        return $this->signArray;
    }

    public function getUserName() {
        if (empty($this->user_name))
            return $this->user_name = 'Гость';
        return $this->user_name;
    }

    public function runCurl($sign) {
        $url = 'http://horoscope.ra-project.net/api/' . $sign;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    function definitionSign($month, $day) {
        $signs = array('10', '11', '12', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $signsstart = array(1 => 21, 2 => 20, 3 => 20, 4 => 20, 5 => 20, 6 => 20, 7 => 21, 8 => 22, 9 => 23, 10 => 23, 11 => 23, 12 => 23);
        return $day < $signsstart[$month] ? $signs[$month - 1] : $signs[$month % 12];
    }

    function getMessage() {
        if ($message = SignZodiac::model()->findAll())
            $this->message = $message;
        return $this->message;
    }

    function getSignValue($Sign) {
        foreach ($this->SignArray as $key => $value) {
            if ($Sign == $key)
                return $value;
        }
    }

    public function beforeSave() {
        $this->input_date = date('Y.m.d', strtotime($this->input_date));
        return parent::beforeSave();
    }

    public function afterSave() {
        $this->input_date = $this->correctdate;
        return parent::afterSave();
    }

    public function beforeValidate() {
        $this->user_name = $this->UserName;
        if (!empty($this->sign))
            $this->message = $this->runCurl($this->sign);
        if (!empty($this->input_date) || empty($this->sign)) {
            if (CDateTimeParser::parse($this->input_date, 'd.M.yyyy')) {
                $birthday = explode('.', $this->input_date);
                $this->sign = $this->definitionSign($birthday[1], $birthday[0]);
                $this->message = $this->runCurl($this->sign);
                $this->correctdate = $this->input_date;
            }
        }
        return parent::beforeSave();
    }

}