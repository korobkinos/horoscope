-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Авг 10 2013 г., 21:58
-- Версия сервера: 5.5.32
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `horoscope`
--
CREATE DATABASE IF NOT EXISTS `horoscope` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `horoscope`;

-- --------------------------------------------------------

--
-- Структура таблицы `sign_zodiac`
--

DROP TABLE IF EXISTS `sign_zodiac`;
CREATE TABLE IF NOT EXISTS `sign_zodiac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `input_date` date DEFAULT NULL,
  `sign` varchar(150) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `message` text NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Дамп данных таблицы `sign_zodiac`
--

INSERT INTO `sign_zodiac` (`id`, `timestamp`, `input_date`, `sign`, `user_name`, `message`) VALUES
(75, '2013-08-10 18:55:05', '1970-01-01', '5', 'Гость', '<forecast><item zodiac_id="5" name="лев" date="10.08.2013"><text>День предвещает успех в любви, удачные свидания, культпоходы, вечеринки.  Не исключено неожиданное предложение или встреча с давним партнером по бизнесу.  Уделите особое внимание разминке или откажитесь от занятий и просто прогуляйтесь по свежему воздуху.   Непредвиденные событие может привести к серьезным конфликтам дома.</text></item></forecast>'),
(76, '2013-08-10 19:57:17', '1987-09-12', '6', 'Олег', '<forecast><item zodiac_id="6" name="дева" date="10.08.2013"><text>На сегодня стоит запланировать встречу с представителями государственных контролирующих инстанций.  Возможны деловые встречи или телефонные переговоры с друзьями или родственниками. В конце дня возможен неожиданный поворот событий, будьте начеку - ситуация может выйти из-под контроля.   Вечером хорошо размяться в тренажерном зале.</text></item></forecast>');
