<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?> 
<div id="sign-form">     
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'datepicker',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'htmlOptions' => array('class' => 'well')
    ));
    ?> 
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->label($model, 'user_name'); ?>
    <?php echo $form->textField($model, 'user_name', array('placeholder' => 'Введите имя')) ?>
    <p>Укажите дату рождения:</p>    
    <?php
    $this->widget('application.components.datepicker.WasDatepicker', array(
        'attribute' => 'input_date',
        'model' => $model,
        'options' => array(
            'language' => 'ru',
            'attribute' => 'calendar',
            'autoclose' => 'true',
            'endDate' => date("m.d.Y"),
            'weekStart' => 1,
            'startView' => 2,
            'keyboardNavigation' => true,
            'format' => 'd.m.yyyy',
            'showAnim' => 'fold',
        ),
    ));
    ?>          
    <?php
    $dropList = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'horizontalForm',
        'type' => 'horizontal',
    ));
    ?>
    <?php echo $dropList->dropDownListRow($model, 'sign', $model->SignArray, array('empty' => 'Укажите ваш знак...')); ?>             
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Отправить',
        'type' => 'info',
        'size' => 'large',
        'buttonType' => 'submit'
    ));
    ?> 
    <?php $this->endWidget(); ?>       
</div>
    <?php $this->endWidget(); ?>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_list',
    'template' => '{items}{pager}',
    'pager' => array(
//        'cssFile' => '/css/styles.css'
        'header' => '',
        'cssFile' => false,
        'maxButtonCount' => 25,
        'selectedPageCssClass' => 'active',
        'hiddenPageCssClass' => 'disabled',
        'firstPageCssClass' => 'Назад',
        'lastPageCssClass' => 'Далее',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
    ),
    'htmlOptions' => array('class' => 'listpage'),
));
?>


