<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<div class="span8">    
    <?php if ($request): ?>
        <h4>Здраствуйте <?php echo $request->user_name ?> (<?php echo SignZodiac::model()->getSignValue($request->sign) ?>)</h4>        
        <p>Ваш гороскоп на сегодня:</p>
        <p><?php echo $request->message ?></p>
        <div>
            <span class="badge badge-success">Гороскоп на <?php echo $request->timestamp ?></span><div class="pull-right"><span class="badge badge-info"><?php echo CHtml::link('Назад', array('Site/index/')); ?></span></div>
        </div> 
        <hr>    
    <?php else: ?>
        <h3>К сожалению, по вашему запросу ничего не найдено</h3>
    <?php endif; ?>
</div>